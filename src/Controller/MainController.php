<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\News;
use App\Repository\CategoryRepository;
use App\Repository\CommentRepository;
use App\Repository\NewsRepository;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class MainController extends Controller
{
    /**
     * @Method({"GET","POST"})
     * @Route("/", name="main_index")
     * @param NewsRepository $newsRepository
     * @param UserRepository $userRepository
     * @param CommentRepository $commentRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction( NewsRepository $newsRepository, UserRepository $userRepository,
                                CommentRepository $commentRepository, CategoryRepository $categoryRepository)
    {
        $news = [];
        $newsData = $newsRepository->getAllNews();
        foreach ($newsData as $key => $value) {
            $arr['id'] = $value['id'] ;
            $arr['title'] = $value['title'] ;
            $arr['description'] = $value['description'] ;
            $arr['createAt'] = $value['createAt'] ;
            $arr['dateOfPublication'] = $value['dateOfPublication'] ;
            $username = $userRepository->find($value['user']);
            $arr['username'] =  $username->getUsername();
            $arr['comments'] = $commentRepository->findByComments($value['id']);
            $arr['category'] = $newsRepository->findByOne($value['id'])->getCategories()[0]->getTitle();
            $news[] = $arr;
        }


        return $this->render("main/index.html.twig", [
            'news' => $news
        ]);
    }

    /**
     * @Route("/create_news", name="create_news")
     * @param Request $request
     * @param ObjectManager $manager
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createNewsAction(Request $request, ObjectManager $manager, CategoryRepository $categoryRepository,
                                     TagRepository $tagRepository)
    {
        $news = new News();
        $user = $this->getUser();
        $data = $request->request->all();
        $categories = $categoryRepository->findAll() ;
        $tags = $tagRepository->findAll() ;
        if(!empty($data['description']) ||  !empty($data['title']) ){
            $categoriesEntity = $categoryRepository->find($data['category']);
            $tagsEntity = $tagRepository->find($data['tags']);
            if($data['tags'] === 'null'){
                $news->setUser($user->getId())
                    ->setTitle($data['title'])
                    ->setDescription($data['description'])
                    ->setCreateAt(new \DateTime())
                    ->addCategories($categoriesEntity);
                $manager->persist($news);
            }else {
                $news->setUser($user->getId())
                    ->setTitle($data['title'])
                    ->setDescription($data['description'])
                    ->setCreateAt(new \DateTime())
                    ->addCategories($categoriesEntity)
                    ->addTags($tagsEntity);
                $manager->persist($news);
            }
            $manager->flush();

            return $this->redirect('/');
        }


        return $this->render("news/create.html.twig", [
            'categories' => $categories,
            'tags' => $tags
        ]);
    }

    /**
     * @Route("/create_comment", name="create_comment")
     */
    public function createCommentAjaxAction(Request $request, NewsRepository $newsRepository,
                                            ObjectManager $manager, CommentRepository $commentRepository)
    {
        $data = $request->request->all();
        $user =$this->getUser();
        if($data){
            $comment = new Comment();
            $news = $newsRepository->find($data['newsId']);
            $user = $this->getUser();
            $username = $user->getUsername();

            $comment->setCreateAt(new \DateTime())
                ->setUser($user)
                ->setComments($data['comment'])
                ->setNews($news);
            $manager->persist($comment);
            $manager->flush();

            return new JsonResponse($username);
        }

        return  new JsonResponse(false);
    }

    /**
     * @Route("/{id}/show", name="news_show")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(int $id, NewsRepository $newsRepository)
    {
        $news = $newsRepository->findByOne($id);

        return $this->render('main/show.html.twig',[
            'news' => $news
        ]);
    }

    /**
     * @Route("/{id}/public", name="news_public")
     * @param int $id
     * @param NewsRepository $newsRepository
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function datePublicationAction(int $id, NewsRepository $newsRepository, Request $request, ObjectManager $manager)
    {
        $news = $newsRepository->findByOne($id);
        $date = $request->request->all();
        $news->setDateOfPublication(new \DateTime($date['calendar']));
        $manager->persist($news);
        $manager->flush();
        return $this->render('main/show.html.twig',[
            'news' => $news
        ]);
    }
}
