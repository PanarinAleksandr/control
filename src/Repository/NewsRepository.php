<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, News::class);
    }

    /**
     * @param int $id
     * @return News|null
     */
    public function findByOne(int $id)
    {
        try {
            return $this->createQueryBuilder('n')
                ->andWhere('n.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return array
     */

    public function getAllNews()
    {
        try {
            return $this->createQueryBuilder('n')
                ->getQuery()
                ->getArrayResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param Category $category
     * @return array
     */

    public function findAllPublishedNews($category = null)
    {
        $qb = $this->createQueryBuilder('n')
            ->where('n.publishedDate <= CURRENT_DATE()')
            ->andWhere('n.publishedDate is NOT NULL');
        if($category){
            $qb
                ->andWhere('n.category =:category')
                ->setParameter('category',$category);
        };
        return $qb
            ->getQuery()
            ->getResult();
    }


}
