<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;


    /**
     * @var News[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\News", mappedBy="categories")
     */
    private $news;

    public function __construct()
    {
        $this->news = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function addNews(News $news)
    {
        if (!$this->news->contains($news)) {
            $this->news[] = $news;
            $news->addCategories($this);
        }

        return $this;
    }

    public function removeNews(News $news)
    {
        if ($this->news->contains($news)) {
            $this->news->removeElement($news);
            $news->removeCategory($this);
        }

        return $this;
    }

    /**
     * @return News[]|ArrayCollection
     */
    public function getNews()
    {
        return $this->news;
    }

}
