<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 */
class Tag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     *  @var News[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\News", mappedBy="tags")
     */
    private $news;

    public function __construct()
    {
        $this->news = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param News[]|ArrayCollection $news
     * @return Tag
     */
    public function setNews($news)
    {
        $this->news = $news;
        return $this;
    }

    /**
     * @return News[]|ArrayCollection
     */
    public function getNews()
    {
        return $this->news;
    }

}
