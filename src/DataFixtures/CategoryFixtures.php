<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1->setTitle('Спорт');
        $manager->persist($category1);

        $category2 = new Category();
        $category2->setTitle('Искуство');
        $manager->persist($category2);

        $category3 = new Category();
        $category3->setTitle('Политика');
        $manager->persist($category3);

        $category4 = new Category();
        $category4->setTitle('Разное');
        $manager->persist($category4);

        $manager->flush();

//        $this->addReference(self::USER_ONE, $user1);
//        $this->addReference(self::USER_TWO, $user2);
//        $this->addReference(self::USER_THREE, $user3);
    }
}