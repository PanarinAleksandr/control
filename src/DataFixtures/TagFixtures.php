<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TagFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $tag1 = new Tag();
        $tag1->setTitle('Бокс');
        $manager->persist($tag1);

        $tag2 = new Tag();
        $tag2->setTitle('Галереи');
        $manager->persist($tag2);

        $tag3 = new Tag();
        $tag3->setTitle('Президент');
        $manager->persist($tag3);

        $tag4 = new Tag();
        $tag4->setTitle('Гонка');
        $manager->persist($tag4);

        $manager->flush();

//        $this->addReference(self::USER_ONE, $user1);
//        $this->addReference(self::USER_TWO, $user2);
//        $this->addReference(self::USER_THREE, $user3);
    }
}