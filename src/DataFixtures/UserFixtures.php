<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    const USER_ONE = 'user1@some.com';
    const USER_TWO = 'user2@some.com';
    const USER_THREE = 'user3@some.com';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $superAdmin = new User();
        $superAdmin
            ->setUsername("admin")
            ->setRoles(['ROLE_ADMIN'])
            ->setEnabled(true)
            ->setEmail("admin@admin.ru")
            ->setPlainPassword('12345')
            ->setSuperAdmin(true)
        ;
        $manager->persist($superAdmin);

        $user1 = new User();
        $user1
            ->setUsername('Ivan')
            ->setEnabled(true)
            ->setEmail(self::USER_ONE)
            ->setPlainPassword('123456')
        ;
        $manager->persist($user1);

        $user2 = new User();
        $user2
            ->setUsername('Petr')
            ->setEnabled(true)
            ->setEmail(self::USER_TWO)
            ->setPlainPassword('1234567')
        ;
        $manager->persist($user2);

        $user3 = new User();
        $user3
            ->setUsername("Igor")
            ->setEnabled(true)
            ->setEmail(self::USER_THREE)
            ->setPlainPassword('123321')
        ;
        $manager->persist($user3);

        $manager->flush();

        $this->addReference(self::USER_ONE, $user1);
        $this->addReference(self::USER_TWO, $user2);
        $this->addReference(self::USER_THREE, $user3);
    }
}